package com.mezzuk.pattern.memento;

import com.codewithmosh.pattern.memento.DocumentState;

import java.util.ArrayList;
import java.util.List;

public class History {
    private List<DocumentState> states = new ArrayList<>();

    public void push(DocumentState state){
        states.add(state);
    }

    public DocumentState pop(){
        var lastIndex = states.size() - 2;
        var lastState = states.get(lastIndex);
        if(lastIndex > 0) {
            states.remove(lastState);
        }

        return lastState;
    }
}
