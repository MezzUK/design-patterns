package com.mezzuk.pattern.memento;

import com.codewithmosh.pattern.memento.DocumentState;

public class Document {
    private String content;
    private String fontName;
    private int fontSize;

    public DocumentState createState(){
        return new DocumentState(content, fontName, fontSize);
    }

    public void restore(DocumentState state){
        this.content = state.getContent();
        this.fontName = state.getFontName();
        this.fontSize = state.getFontSize();
    }

    public String getContent() {
        return content;
    }

    public String getFontName(){
        return fontName;
    }

    public int getFontSize() {
        return fontSize;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setFontName(String fontName) {
        this.fontName = fontName;
    }

    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }
}
