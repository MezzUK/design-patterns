package com.mezzuk.pattern;


import com.mezzuk.pattern.memento.Document;
import com.mezzuk.pattern.memento.History;

public class Main {

    public static void main(String[] args) {
	MementoPattern();
    }

    private static void MementoPattern() {
        var document = new Document();
        var history = new History();

        document.setContent("Content 1");
        document.setFontName("Font 1");
        document.setFontSize(1);
        history.push(document.createState());

        document.setContent("Content 2");
        document.setFontName("Font 2");
        document.setFontSize(2);
        history.push(document.createState());

        document.setContent("Content 3");
        document.setFontName("Font 3");
        document.setFontSize(3);
        history.push(document.createState());

        document.restore(history.pop());
        document.restore(history.pop());
        document.restore(history.pop());

        System.out.println(document.getContent());
        System.out.println(document.getFontName());
        System.out.println(document.getFontSize());
    }
}